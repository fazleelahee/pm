-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.31-0ubuntu0.12.04.2 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for paymate
DROP DATABASE IF EXISTS `paymate`;
CREATE DATABASE IF NOT EXISTS `paymate` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `paymate`;


-- Dumping structure for table paymate.account_holder
DROP TABLE IF EXISTS `account_holder`;
CREATE TABLE IF NOT EXISTS `account_holder` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `currency_id` int(10) NOT NULL DEFAULT '0',
  `account_type_id` int(10) NOT NULL DEFAULT '0',
  `title` varchar(10) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1_currency` (`currency_id`),
  KEY `FK2_account_type` (`account_type_id`),
  CONSTRAINT `FK1_currency` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`),
  CONSTRAINT `FK2_account_type` FOREIGN KEY (`account_type_id`) REFERENCES `account_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table paymate.account_holder_address
DROP TABLE IF EXISTS `account_holder_address`;
CREATE TABLE IF NOT EXISTS `account_holder_address` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `account_holder_id` int(11) NOT NULL DEFAULT '0',
  `address_line` varchar(50) NOT NULL DEFAULT '0',
  `address_line_1` varchar(50) NOT NULL DEFAULT '0',
  `address_line_2` varchar(50) NOT NULL DEFAULT '0',
  `city` varchar(50) NOT NULL DEFAULT '0',
  `country` varchar(50) NOT NULL DEFAULT '0',
  `is_current` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK__account_holder` (`account_holder_id`),
  CONSTRAINT `FK__account_holder` FOREIGN KEY (`account_holder_id`) REFERENCES `account_holder` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table paymate.account_type
DROP TABLE IF EXISTS `account_type`;
CREATE TABLE IF NOT EXISTS `account_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table paymate.balance_sheet
DROP TABLE IF EXISTS `balance_sheet`;
CREATE TABLE IF NOT EXISTS `balance_sheet` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `account_holder_id` int(10) DEFAULT NULL,
  `transaction_id` int(10) DEFAULT NULL,
  `description` text,
  `debit` double DEFAULT NULL,
  `credit` double DEFAULT NULL,
  `balance` double DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1_account_balance` (`account_holder_id`),
  KEY `FK2_balance_transaction` (`transaction_id`),
  CONSTRAINT `FK2_balance_transaction` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`transaction_id`),
  CONSTRAINT `FK1_account_balance` FOREIGN KEY (`account_holder_id`) REFERENCES `account_holder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table paymate.currency
DROP TABLE IF EXISTS `currency`;
CREATE TABLE IF NOT EXISTS `currency` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `currency` varchar(50) DEFAULT NULL,
  `code` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table paymate.event_log
DROP TABLE IF EXISTS `event_log`;
CREATE TABLE IF NOT EXISTS `event_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `event_code` int(10) DEFAULT NULL,
  `message` text,
  `date_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table paymate.login_audit
DROP TABLE IF EXISTS `login_audit`;
CREATE TABLE IF NOT EXISTS `login_audit` (
  `id` int(10) NOT NULL,
  `account_holder_id` int(10) DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__account_holder_login` (`account_holder_id`),
  CONSTRAINT `FK__account_holder_login` FOREIGN KEY (`account_holder_id`) REFERENCES `account_holder` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table paymate.transaction
DROP TABLE IF EXISTS `transaction`;
CREATE TABLE IF NOT EXISTS `transaction` (
  `transaction_id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_from` varchar(128) DEFAULT NULL,
  `payment_to` varchar(128) DEFAULT NULL,
  `base_amount` double DEFAULT NULL,
  `currency_conversion_rate` double DEFAULT NULL,
  `commission_rate` double DEFAULT NULL,
  `commission_amount` double DEFAULT NULL,
  `currency_conversion_amount` double DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  `approved` tinyint(4) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `date_time_approved` datetime DEFAULT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
